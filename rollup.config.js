import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import progress from 'rollup-plugin-progress';

export default {
  entry : 'src/main.js',
  dest : 'dist/bundle.js',
  format : 'cjs',
  plugins : [
    babel({
      presets: [
        'stage-0', 'babili'
      ],
      plugins: [
        'transform-class-properties', [
          'transform-react-jsx', {
            'pragma': 'h'
          }
        ]
      ],
      comments: false
    }),
    commonjs({
      include: 'src',
      extensions: [
        '.js', '.jsx'
      ],
      sourceMap: false
    }),
    resolve({
      jsnext: true,
      main: true
    }),
    progress({
      clearLine: false
    })
  ]
};
